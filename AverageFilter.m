function AverageFilter
count = 1;

%% read an image
img = imread('.\res\lena_noiseImg.jpg');
%img = rgb2gray(img);

%% get size
[nrow, ncol] = size(img);

%% generate new img
img = cast(img, 'double');
newImg = zeros(nrow,ncol);

%% image operation
for cnt = 1:count
    for i = 2:nrow-1
        for j = 2:ncol-1
            sub = img(i-1:i+1,j-1:j+1);
            img(i,j) = sum(sum(sub))/9;
        end
    end
end

newImg = cast(newImg, 'uint8');
imshow(newImg);

end