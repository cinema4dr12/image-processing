function HomogeneityOperator
%% read an image
img = imread('.\res\test_02.jpg');
subplot(2,1,1);
imshow(img);

img = rgb2gray(img);

[nrow, ncol] = size(img);
img = cast(img, 'double');
newImg = zeros(nrow,ncol);

% Corners
i = 1; j = 1;
tmp(1) = abs(img(i,j) - img(i,j+1));
tmp(2) = abs(img(i,j) - img(i+1,j+1));
tmp(3) = abs(img(i,j) - img(i+1,j));
newImg(i,j) = max(tmp);

i = 1; j = ncol;
tmp(1) = abs(img(i,j) - img(i+1,j));
tmp(2) = abs(img(i,j) - img(i+1,j-1));
tmp(3) = abs(img(i,j) - img(i,j-1));
newImg(i,j) = max(tmp);

i = nrow; j = ncol;
tmp(1) = abs(img(i,j) - img(i,j-1));
tmp(2) = abs(img(i,j) - img(i-1,j-1));
tmp(3) = abs(img(i,j) - img(i-1,j));
newImg(i,j) = max(tmp);

i = nrow; j = 1;
tmp(1) = abs(img(i,j) - img(i-1,j));
tmp(2) = abs(img(i,j) - img(i-1,j+1));
tmp(3) = abs(img(i,j) - img(i,j+1));
newImg(i,j) = max(tmp);

% Edges
i = 1;
for(j = 2:ncol-1)
    tmp(1) = abs(img(i,j) - img(i,j-1));
    tmp(2) = abs(img(i,j) - img(i+1,j-1));
    tmp(3) = abs(img(i,j) - img(i+1,j));
    tmp(4) = abs(img(i,j) - img(i+1,j+1));
    tmp(5) = abs(img(i,j) - img(i,j+1));
    newImg(i,j) = max(tmp);
end

i = nrow;
for(j = 2:ncol-1)
    tmp(1) = abs(img(i,j) - img(i,j-1));
    tmp(2) = abs(img(i,j) - img(i-1,j-1));
    tmp(3) = abs(img(i,j) - img(i-1,j));
    tmp(4) = abs(img(i,j) - img(i-1,j+1));
    tmp(5) = abs(img(i,j) - img(i,j+1));
    newImg(i,j) = max(tmp);
end

j = 1;
for(i = 2:nrow-1)
    tmp(1) = abs(img(i,j) - img(i-1,j));
    tmp(2) = abs(img(i,j) - img(i-1,j+1));
    tmp(3) = abs(img(i,j) - img(i,j+1));
    tmp(4) = abs(img(i,j) - img(i+1,j+1));
    tmp(5) = abs(img(i,j) - img(i+1,j));
    newImg(i,j) = max(tmp);
end

j = ncol;
for(i = 2:nrow-1)
    tmp(1) = abs(img(i,j) - img(i+1,j));
    tmp(2) = abs(img(i,j) - img(i+1,j-1));
    tmp(3) = abs(img(i,j) - img(i,j-1));
    tmp(4) = abs(img(i,j) - img(i-1,j-1));
    tmp(5) = abs(img(i,j) - img(i-1,j));
    newImg(i,j) = max(tmp);
end


% Inner
for(i = 2:nrow-1)
    for(j = 2:ncol-1)
        tmp(1) = abs(img(i,j)-img(i-1,j-1));
        tmp(2) = abs(img(i,j)-img(i-1,j));
        tmp(3) = abs(img(i,j)-img(i-1,j+1));
        tmp(4) = abs(img(i,j)-img(i,j-1));
        tmp(5) = abs(img(i,j)-img(i,j+1));
        tmp(6) = abs(img(i,j)-img(i+1,j-1));
        tmp(7) = abs(img(i,j)-img(i+1,j));
        tmp(8) = abs(img(i,j)-img(i+1,j+1));
        newImg(i,j) = max(tmp);
    end
end

subplot(2,1,2);
newImg = cast(newImg, 'uint8');
imshow(newImg);

end