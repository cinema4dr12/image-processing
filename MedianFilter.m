function MedianFilter
count = 20;
baseFilename = '.\result\noise';

%% read an image
img = imread('.\res\lena_noiseImg.jpg');
%img = rgb2gray(img);

%% get size
[nrow, ncol] = size(img);

%% generate new img
img = cast(img, 'double');
newImg = zeros(nrow,ncol);

%% image operation
for cnt = 1:count
    for i = 2:nrow-1
        for j = 2:ncol-1
            sub = img(i-1:i+1,j-1:j+1);
            vec = [ sub(1,:) sub(2,:) sub(3,:) ];
            sorted = sort(vec);
            newImg(i,j) = sorted(5);
        end
    end
    
    img = newImg;
    
    % save
    newImg = cast(newImg, 'uint8');
    
    if( length(num2str(cnt)) == 1 )
        filename = strcat( baseFilename, '0' );
        filename = strcat( filename, num2str(cnt) );
        imwrite(newImg, filename);
    else
        filename = strcat( filename, num2str(cnt) );
        imwrite(newImg, filename);
    end
    
end

imshow(newImg);
end