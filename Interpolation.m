function Interpolation
%% read an image
img = imread('.\res\test_02.jpg');

%subplot(2,1,1);
%imshow(img);

[nrow, ncol, tmp] = size(img);
img = cast(img, 'double');
newImg = zeros(nrow,ncol);

scale = 2;

% Filling-in
newImg = zeros(2*nrow, 2*ncol, tmp);
for(i = 1:nrow)
    for(j = 1:ncol)
        newImg(2*(i-1)+1, 2*(j-1)+1, :) = img(i,j,:);
    end
end

% Corners



% Edges


% Inner
for(i = 2:nrow-1)
    for(j = 2:ncol-1)
        newImg(2*i,2*j,:) = ( img(i-1,j-1,:) + img(i-1,j,:) + img(i-1,j+1,:) + img(i,j+1,:) + img(i+1,j+1,:) + img(i+1,j,:) + img(i+1,j-1,:) + img(i,j-1,:) ) / 8;
    end
end

%subplot(2,1,2);
newImg = cast(newImg, 'uint8');
imshow(newImg);

end