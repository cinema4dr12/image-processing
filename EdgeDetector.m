function EdgeDetector
%% read an image
img = imread('.\res\test_02.jpg');

img = rgb2gray(img);
subplot(2,1,1);
imshow(img);

mask = [0 -1 0 ; -1 4 -1 ; 0 -1 0];
[rSize, cSize] = size(mask);

[nrow, ncol] = size(img);
img = cast(img, 'double');
newImg = zeros(nrow,ncol);

% Corners
subMask = mask(2:3,2:3);
i = 1;
j = 1;
subMat = img(i:i+1,j:j+1);
newImg(i,j) = sum(sum(subMask.*subMat)) + 128;

subMask = mask(1:2,1:2);
i = nrow;
j = ncol;
subMat = img(i-1:i,j-1:j);
newImg(i,j) = sum(sum(subMask.*subMat)) + 128;


% Edges
subMask = mask(2:3,1:3);
i = 1;
for(j = 2:ncol-1)
    subMat = img(i:i+1,j-1:j+1);
    newImg(i,j) = sum(sum(subMask.*subMat)) + 128;
end

subMask = mask(1:2,1:3);
i = nrow;
for(j = 2:ncol-1)
    subMat = img(i-1:i,j-1:j+1);
    newImg(i,j) = sum(sum(subMask.*subMat)) + 128;
end

subMask = mask(1:3,2:3);
j = 1;
for(i = 2:nrow-1)
    subMat = img(i-1:i+1,j:j+1);
    newImg(i,j) = sum(sum(subMask.*subMat)) + 128;
end

subMask = mask(1:3,1:2);
j = ncol;
for(i = 2:nrow-1)
    subMat = img(i-1:i+1,j-1:j);
    newImg(i,j) = sum(sum(subMask.*subMat)) + 128;
end

% Inner
for(i = 0.5*(rSize+1) : nrow - 0.5*(rSize+1))
    for(j = 0.5*(cSize+1) : ncol - 0.5*(cSize+1))
        subMat = img(i-1:i+1,j-1:j+1);
        newImg(i,j) = sum(sum(subMat.*mask)) + 128;
    end
end

subplot(2,1,2);
newImg = cast(newImg, 'uint8');
imshow(newImg);

end