function Hist
%% read an image
img = imread('.\res\vectorc.jpg');

img = rgb2gray(img);
[nrow, ncol] = size(img);

subplot(1,2,1);
imshow(img);

%% vectorize img to tmp
tmp = [];
for(i = 1:nrow)
    tmp = [tmp img(i,:)];
end
tmp = cast(tmp,'double');

%% histogram
histo = zeros(256,1);
MIN = 0;
MAX = 0;
for(i = 1:length(tmp))
    histo(tmp(i)+1) = histo(tmp(i)+1) + 1;
    if(MIN > tmp(i))
        MIN = tmp(i);
    end
    if(MAX < tmp(i))
        MAX = tmp(i);
    end
end


%% accumulation of frequency
dummy = 0;
freq = zeros(length(histo),1);
for(i = 1:length(histo))
    dummy = dummy + histo(i);
    freq(i) = dummy;
end

%% normalization
normal = zeros(length(freq),1);
Imax = 255;
N = nrow * ncol;
for(i = 1:length(freq))
    val = freq(i) * Imax / N;
    normal(i) = roundoff(val);
end

%% mapping
newImg = tmp;
for(i = 1:length(tmp))
    newImg(i) = normal(tmp(i)+1);
end

%% separation
for(i = i:nrow)
    img(i,:) = newImg((i-1)*ncol+1:i*ncol);
end

img = cast(img,'uint8');

subplot(1,2,2);
imshow(img);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rndoff = roundoff(num)
int = cast(num,'uint8');
deci = num - cast(cast(num,'uint8'),'double');
if(deci < 0.5)
    rndoff = cast(int,'double');
else
    rndoff = cast(int+1,'double');
end
end