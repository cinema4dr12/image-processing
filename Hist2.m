function Hist2
%% read an image
img = imread('.\res\vectorc.jpg');

[nrow, ncol, dummy] = size(img);

subplot(1,2,1);
imshow(img);

%% vectorize img to tmp
tmpR = [];
tmpG = [];
tmpB = [];
for(i = 1:nrow)
    tmpR = [tmpR img(i,:,1)];
    tmpG = [tmpG img(i,:,2)];
    tmpB = [tmpB img(i,:,3)];
end
tmpR = cast(tmpR,'double');
tmpG = cast(tmpG,'double');
tmpB = cast(tmpB,'double');

%% histogram
histoR = zeros(256,1);
histoG = zeros(256,1);
histoB = zeros(256,1);
MINR = 0; MAXR = 0;
MING = 0; MAXG = 0;
MINB = 0; MAXB = 0;
for(i = 1:length(tmpR))
    histoR(tmpR(i)+1) = histoR(tmpR(i)+1) + 1;
    histoG(tmpG(i)+1) = histoG(tmpG(i)+1) + 1;
    histoB(tmpB(i)+1) = histoB(tmpB(i)+1) + 1;
    
    if(MINR > tmpR(i))
        MINR = tmpR(i);
    end
    if(MAXR < tmpR(i))
        MAXR = tmpR(i);
    end
    
    if(MING > tmpG(i))
        MING = tmpG(i);
    end
    if(MAXG < tmpG(i))
        MAXG = tmpG(i);
    end
    
    if(MINB > tmpB(i))
        MINB = tmpB(i);
    end
    if(MAXB < tmpB(i))
        MAXB = tmpB(i);
    end
end


%% accumulation of frequency
dummyR = 0;
dummyG = 0;
dummyB = 0;
freqR = zeros(length(histoR),1);
freqG = zeros(length(histoG),1);
freqB = zeros(length(histoB),1);
for(i = 1:length(histoR))
    dummyR = dummyR + histoR(i);
    dummyG = dummyG + histoG(i);
    dummyB = dummyB + histoB(i);
    
    freqR(i) = dummyR;
    freqG(i) = dummyG;
    freqB(i) = dummyB;
end

%% normalization
normalR = zeros(length(freqR),1);
normalG = zeros(length(freqG),1);
normalB = zeros(length(freqB),1);
Imax = 255;
N = nrow * ncol;
for(i = 1:length(freqR))
    valR = freqR(i) * Imax / freqR(length(freqR));
    valG = freqG(i) * Imax / freqG(length(freqG));
    valB = freqB(i) * Imax / freqB(length(freqB));
    
    normalR(i) = roundoff(valR);
    normalG(i) = roundoff(valG);
    normalB(i) = roundoff(valB);
end

%% mapping
newImgR = tmpR;
newImgG = tmpG;
newImgB = tmpB;
for(i = 1:length(tmpR))
    newImgR(i) = normalR(tmpR(i)+1);
    newImgG(i) = normalG(tmpG(i)+1);
    newImgB(i) = normalB(tmpB(i)+1);
end

%% separation
imgR = zeros(nrow, ncol);
imgG = zeros(nrow, ncol);
imgB = zeros(nrow, ncol);

for(i = i:nrow)
    imgR(i,:) = newImgR((i-1)*ncol+1:i*ncol);
    imgG(i,:) = newImgG((i-1)*ncol+1:i*ncol);
    imgB(i,:) = newImgB((i-1)*ncol+1:i*ncol);
end

imgR = cast(imgR,'uint8');
imgG = cast(imgG,'uint8');
imgB = cast(imgB,'uint8');

img(:,:,1) = imgR;
img(:,:,2) = imgG;
img(:,:,3) = imgB;

subplot(1,2,2);
imshow(img);

en