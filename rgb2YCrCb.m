function rgb2YCrCb

img = imread('.\res\vectorc.jpg');
[nrow, ncol, dummy] = size(img);

Y = 0.299*img(:,:,1) + 0.587*img(:,:,2) + 0.114*img(:,:,3);
Cr = 0.5*img(:,:,1) - 0.419*img(:,:,2) - 0.0813*img(:,:,3);
Cb = -0.169*img(:,:,1) - 0.331*img(:,:,2) +0.5*img(:,:,3);

hold on;
imshow(Y);
imshow(Cr);
imshow(Cb);

end