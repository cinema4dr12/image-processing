function rndoff = roundoff(num)
int = cast(num,'uint8');
deci = num - cast(cast(num,'uint8'),'double');
if(deci < 0.5)
    rndoff = cast(int,'double');
else
    rndoff = cast(int+1,'double');
end
end