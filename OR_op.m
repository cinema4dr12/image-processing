function OR_op

img = imread('.\res\vectorc.jpg');
[nrow, ncol, dummy] = size(img);

mask = imread('.\res\mask.png');
mask = rgb2gray(mask);

img = cast(img,'double');
mask = cast(mask,'double');
%mask = mask/255;

img(:,:,1) = img(:,:,1) + mask;
img(:,:,2) = img(:,:,2) + mask;
img(:,:,3) = img(:,:,3) + mask;

img = cast(img,'uint8');
imshow(img);

end